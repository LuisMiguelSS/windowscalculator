﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Calculadora.src
{
    /// <summary>
    /// Intérprete de expresiones matemáticas.
    /// La autoría original de este código corresponde a "Alberto Población",
    /// pero ambas modificaciones y refactorizaciones corresponden a "Luis Miguel
    /// Soto Sánchez".
    /// Este sencillo intérprete no hace más que realizar
    /// las operaciones matemáticas dadas en una cadena.
    /// </summary>
    public class Interprete
    {
        //
        // Atributos
        //
        public enum Simbolo
        {
            Suma, Resta,
            Multiplicacion, Division,
            ParentesisAbierto, ParentesisCerrado,
            Constante, Variable,
            FinExpresion
        }

        private string ExpresionMatematica;
        private int Posicion;
        private Simbolo UltimoSimbolo;
        private double UltimaConstante;
        private Stack<double> Valores;
        private double ValorX;

        private const int VALOR_DEFECTO_X = 0;

        //
        // Constructor
        //
        public Interprete(string expresion)
        {
            ExpresionMatematica = PrepararExpresion(expresion);
            Resetear();
        }

        //
        // Otros métodos
        //
        private string PrepararExpresion(string expresion)
        {
            string nuevaExpresion = expresion;

            if (nuevaExpresion.StartsWith("-"))
                nuevaExpresion = 0 + nuevaExpresion;

            return nuevaExpresion.Replace(" ", "")
                            .Replace(",", ".")
                            .Replace("--", "+")
                            .Replace("+-", "-")
                            .Replace("-+", "-")
                            .Replace("++", "+");
        }
        private void Resetear(double valorX = VALOR_DEFECTO_X)
        {
            ValorX = valorX;
            Posicion = 0;
            Valores = new Stack<double>();
            UltimoSimbolo = GetSiguienteSimbolo();
        }

        public double Evaluar(double x = VALOR_DEFECTO_X)
        {
            Resetear(x);

            Expresion();

            if (Valores.Count < 1)
                throw new Exception("Error al realizar operaciones con la pila de datos.");

            return Valores.Pop();
        }
        private Simbolo GetSiguienteSimbolo()
        {
            if (Posicion >= ExpresionMatematica.Length)
                return Simbolo.FinExpresion;

            char caracter = ExpresionMatematica[Posicion++];

            switch (caracter)
            {
                case 'x': case 'X': return Simbolo.Variable;
                case '(': return Simbolo.ParentesisAbierto;
                case ')': return Simbolo.ParentesisCerrado;
                case '+': return Simbolo.Suma;
                case '-': return Simbolo.Resta;
                case '*': return Simbolo.Multiplicacion;
                case '/': case '÷': return Simbolo.Division;
            }

            Regex RegexFormatoCorrecto = new Regex(@"^\d+([,\.]\d+)?");
            string exp = ExpresionMatematica.Substring(Posicion - 1);

            if (RegexFormatoCorrecto.IsMatch(exp))
            {
                Match m = RegexFormatoCorrecto.Match(exp);
                Posicion += m.Length - 1;
                UltimaConstante = double.Parse(m.Value.Replace(".", ","));
                return Simbolo.Constante;
            }

            throw new Exception("Símbolo no reconocido en la posición " + Posicion + ".");
        }
        private void Expresion()
        {
            Termino();
            while (true)
            {
                switch (UltimoSimbolo)
                {
                    case Simbolo.Suma:
                        UltimoSimbolo =
                        GetSiguienteSimbolo();
                        Termino();
                        Sumar();
                        break;
                    case Simbolo.Resta:
                        UltimoSimbolo =
                        GetSiguienteSimbolo();
                        Termino();
                        Restar();
                        break;
                    default: return;
                }
            }
        }
        private void Termino()
        {
            Factor();
            while (true)
            {
                switch (UltimoSimbolo)
                {
                    case Simbolo.Multiplicacion:
                        UltimoSimbolo =
                        GetSiguienteSimbolo();
                        Factor();
                        Multiplicar();
                        break;
                    case Simbolo.Division:
                        UltimoSimbolo =
                        GetSiguienteSimbolo();
                        Factor();
                        Dividir();
                        break;
                    default: return;
                }
            }
        }
        private void Factor()
        {
            if (UltimoSimbolo == Simbolo.ParentesisAbierto)
            {
                UltimoSimbolo = GetSiguienteSimbolo();
                Expresion();
                if (UltimoSimbolo != Simbolo.ParentesisCerrado)
                    throw new Exception("Falta ')'");
                UltimoSimbolo = GetSiguienteSimbolo();
            }
            else if (UltimoSimbolo == Simbolo.Constante)
            {
                OperacionConstante();
                UltimoSimbolo = GetSiguienteSimbolo();
            }
            else if (UltimoSimbolo == Simbolo.Variable)
            {
                OperacionVariable();
                UltimoSimbolo = GetSiguienteSimbolo();
            }
            else
                throw new Exception("El formato de la entrada recibida es erróneo.");
        }
        private void OperacionConstante()
        {
            Valores.Push(UltimaConstante);
        }
        private void OperacionVariable()
        {
            Valores.Push(ValorX);
        }
        private void Sumar()
        {
            Valores.Push(Valores.Pop() + Valores.Pop());
        }
        private void Restar()
        {
            double op2 = Valores.Pop();
            double op1 = Valores.Pop();
            Valores.Push(op1 - op2);
        }
        private void Multiplicar()
        {
            Valores.Push(Valores.Pop() * Valores.Pop());
        }
        private void Dividir()
        {
            double op2 = Valores.Pop();
            double op1 = Valores.Pop();
            Valores.Push(op1 / op2);
        }
    }
}
