﻿using Calculadora.src;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace Calculadora
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            // Números
            Button0.Click += ButtonNumero_Click;
            Button0.MouseEnter += (s, e) => { };
            Button1.Click += ButtonNumero_Click;
            Button2.Click += ButtonNumero_Click;
            Button3.Click += ButtonNumero_Click;
            Button4.Click += ButtonNumero_Click;
            Button5.Click += ButtonNumero_Click;
            Button6.Click += ButtonNumero_Click;
            Button7.Click += ButtonNumero_Click;
            Button8.Click += ButtonNumero_Click;
            Button9.Click += ButtonNumero_Click;

            // Símbolos
            ButtonBorrarTodo.Click += (s, e) => { TextBoxResultado.Text = "0"; };
            ButtonBorrarCaracter.Click += (s, e) =>
            {
                if (TextBoxResultado.Text.Length > 1)
                    TextBoxResultado.Text = TextBoxResultado.Text.Remove(TextBoxResultado.Text.Length - 1);
                else
                    TextBoxResultado.Text = "0";
            };

            ButtonComa.Click += ButtonSimbolo_Click;
            ButtonMas.Click += ButtonSimbolo_Click;
            ButtonMenos.Click += ButtonSimbolo_Click;
            ButtonMultiplicar.Click += ButtonSimbolo_Click;
            ButtonDividir.Click += ButtonSimbolo_Click;
        }

        private void ButtonIgual_Click(object sender, RoutedEventArgs e)
        {
            if (char.IsDigit(TextBoxResultado.Text[TextBoxResultado.Text.Length - 1]))
            {
                LabelCalculoAnterior.Content = TextBoxResultado.Text;
                TextBoxResultado.Text = new Interprete(TextBoxResultado.Text).Evaluar().ToString();
            }
        }

        private void ButtonNumero_Click(object sender, RoutedEventArgs e)
        {
            if (TextBoxResultado.Text == "0")
                TextBoxResultado.Text = ((Button)sender).Content.ToString();
            else
                TextBoxResultado.Text += ((Button)sender).Content;

        }

        private void ButtonSimbolo_Click(object sender, RoutedEventArgs e)
        {
            char ultimoCaracter = TextBoxResultado.Text[TextBoxResultado.Text.Length - 1];
            string valorBoton = ((Button)sender).Content.ToString();

            if (char.IsDigit(ultimoCaracter))
            {
                if (valorBoton == "x")
                    valorBoton = "*";
                else if (valorBoton == "," && new Regex(@".*,\d$").IsMatch(TextBoxResultado.Text))
                    return;

                TextBoxResultado.Text += valorBoton;

            }
        }

        private void ButtonCopiarPortapapeles_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(TextBoxResultado.Text);
        }
    }
}
